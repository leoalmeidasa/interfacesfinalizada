package interface00;

public class Person implements MyInterface, Communicate {

    //Class Atributes
    protected String Name;
    protected int Age;
    protected double Weight;
    protected double Height;
    protected double BMI;
    private String REC_MESSAGE;

    //Constructor
    public Person() {
        REC_MESSAGE="default";
        Name = "";
        Age = 0;
    }//end constructor

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        if (Age >= 0) {
            this.Age = Age;
        } else {
            System.out.println("Age must be grater than zero !");
        }
    }

    public double getWeight() {
        return Weight;
    }

    public void setWeight(double Weight) {
        if (Weight >= 1.0) {
            this.Weight = Weight;
        } else {
            System.out.println("Weight must be greater than 1 Kilogram ! ");
        }

    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        if (Height >= 0.15) {
            this.Height = Height;
        } else {
            System.out.println("Height must be greater than 0.10 meteres ! ");
        }
    }

    public double BMI() {
        BMI = Weight / (Height * Height);
        if (Weight > -1.0 && Height >= 0.15) {
            return BMI;
        }
        return 0.0;
    }

    @Override
    public String toString() {
        return "Person{" + "Name=" + Name + ", Age=" + Age + ", Weight=" + Weight + ", Height=" + Height + ", BMI=" + BMI() + '}';
    }

    @Override
    public void showMessage(String Msg) {
        if (Msg.length() <= MyInterface.MAX_MSG_LENGTH) {
            System.out.println(Msg);
        } else {
            System.out.println("Can't show message size=" + MyInterface.MAX_MSG_LENGTH);
        }
    }

    @Override
    public String getUser() {
        return this.Name;
    }

    @Override
    public void sendMessage(String Msg) {
        this.REC_MESSAGE = Msg;
    }

    @Override
    public String receivedMessage() {
        return REC_MESSAGE;
    }
    
    public String getMessage(){
        return this.REC_MESSAGE;
    }

}//end class
