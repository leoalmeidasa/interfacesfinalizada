package interface00;

import java.util.Scanner;
import javax.swing.JFrame;

public class MainInterface {

    public static void main(String[] args) {
        Scanner read= new Scanner(System.in); 
        MyInterface leo = new Person();
        
        //Can't instanciate from interface
        //MyInterface obj=new MyInterface();
        
        //Object Casting
        ((Person)leo).setName("Leo");
        
        MyInterface desktop = new Form();
        //show the jFrame
        ((JFrame)desktop).setVisible(true);
        
        System.out.print("Digit message to send to object:");
        String Msg= read.nextLine();
        
        //Invoking methods from interface
        leo.showMessage(Msg);
        desktop.showMessage(Msg);
        
        
        //Sendinf message from object to desktop
        ((Communicate)desktop).sendMessage(((Communicate)leo).receivedMessage());
        //Sending message from desktop to object
        ((Communicate)leo).sendMessage(((Communicate)desktop).receivedMessage());
        
        
        System.out.println("Object user:"+leo.getUser());
        System.out.println("Desktop user:"+desktop.getUser());
        System.out.println("Person message:"+((Person)leo).getMessage());
    }
}
