
package interface00;


public interface Communicate {
    
    public void sendMessage (String Msg);
    public String receivedMessage ();
    
}
